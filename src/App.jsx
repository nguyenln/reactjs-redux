import React from "react";
import "./App.css";
import Dashboard from "./app/Dashboard/dashboard";
import Heroes from "./app/Heroes/heroes";
import HeroDetail from "./app/HeroDetail/herodetail";
import MesssageApp from "./app/MesssageApp/message";

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  withRouter
} from "react-router-dom";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hero: {},
      screen: "/dashboard"
    };
  }

  handlerChange(hero, screen) {
    this.setState({ hero: hero, screen: screen });
    console.log(hero);
  }

  render() {
    const title = "Tour of Heroes";

    return (
      <Router>
        <div>
          <h1>{title}</h1>
          <nav>
            <Link to="/dashboard">Dashboard</Link>
            <Link to="/heroes">Heroes</Link>
          </nav>
          <Switch>
            <Route path="/dashboard">
              <Dashboard />
            </Route>
            <Route path="/heroes">
              <Heroes />
            </Route>
            <Route path="/heroesdetail">
              <HeroDetail
                hero={this.state.hero}
                screen={this.state.screen}
              ></HeroDetail>
            </Route>
            <Route path="/">
              <Dashboard />
            </Route>
          </Switch>
          <MesssageApp />
        </div>
      </Router>
    );
  }
}

export default withRouter(App);
