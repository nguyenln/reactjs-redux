import React from 'react';
import './herosearch.css'
import { filterHeroes } from '../redux/actions';
import { connect } from 'react-redux';
import { Link } from "react-router-dom";
import { selectHeroes } from '../redux/actions/index';

class HeroSearch extends React.Component {
    constructor(props) {
        super(props);
        this.filterHandler = this.filterHandler.bind(this);
    }

    selectHandler(event,hero){
        this.props.dispatch(selectHeroes(hero));
      }

    filterHandler(event){
        this.props.dispatch(filterHeroes(event.target.value));
    }

    render() {
        var element = this.props.filterHeroes.map(hero => {
            return (
            <li  key = {hero.id} >
                <Link to='/heroesdetail' onClick={(event,item) => this.selectHandler(event,hero) }>
                    {hero.name}
                </Link>
            </li>
            )
        })
       return (
          <div>
             <h4><label>Hero Search</label></h4>
             <input type="text" name="herosearch" onChange={this.filterHandler}/>
             <ul className="search-result">
                 {element}
             </ul>
          </div>
       )
    }
 }

 const mapStateToProps = (state, ownProps) => {
    return { filterHeroes: state.filterHeroes };
  };
  
  export default connect(mapStateToProps)(HeroSearch);