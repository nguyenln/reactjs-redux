import {
  GET_HEROES_SUCCESS,
  FILTER_HEROES_SUCCESS,
  SELECT_HEROES_SUCCESS,
  ADD_HEROES_SUCCESS,
  UPDATE_HEROES_NAME_SUCCESS,
  DELETE_HEROES_SUCCESS
} from "./action-type";
import axios from "axios";

export function getHeroes() {
  return function(dispatch) {
    return axios.get("http://localhost:15001/api/getlisthero").then(result => {
      dispatch({ type: GET_HEROES_SUCCESS, payload: result.data });
    });
  };
}

export function filterHeroes(name) {
  return function(dispatch) {
    dispatch({ type: FILTER_HEROES_SUCCESS, payload: name });
  };
}

export function selectHeroes(hero) {
  return function(dispatch) {
    dispatch({ type: SELECT_HEROES_SUCCESS, payload: hero });
  };
}

export function addHeroes(hero) {
  return function(dispatch) {
    return axios
      .post("http://localhost:15001/api/addhero", hero)
      .then(result => {
        dispatch({ type: ADD_HEROES_SUCCESS, payload: result.data });
      })
      .then(x => {
        axios.get("http://localhost:15001/api/getlisthero").then(result => {
          dispatch({ type: GET_HEROES_SUCCESS, payload: result.data });
        });
      });
  };
}

export function updateNameHeroes(hero) {
  return function(dispatch) {
    return axios
      .put("http://localhost:15001/api/updatehero/" + hero.id, hero)
      .then(result => {
        dispatch({ type: UPDATE_HEROES_NAME_SUCCESS, payload: result.data });
      });
  };
}

export function deleteHeroes(idx) {
  return function(dispatch) {
    return axios
      .delete("http://localhost:15001/api/deletehero/" + idx)
      .then(result => {
        dispatch({ type: DELETE_HEROES_SUCCESS, payload: result.data });
      })
      .then(x => {
        axios.get("http://localhost:15001/api/getlisthero").then(result => {
          dispatch({ type: GET_HEROES_SUCCESS, payload: result.data });
        });
      });
  };
}
