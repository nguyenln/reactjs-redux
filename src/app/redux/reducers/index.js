import {
  GET_HEROES_SUCCESS,
  FILTER_HEROES_SUCCESS,
  SELECT_HEROES_SUCCESS,
  ADD_HEROES_SUCCESS,
  DELETE_HEROES_SUCCESS,
  UPDATE_HEROES_NAME_SUCCESS
} from "../actions/action-type";

const initialState = {
  heroes: [],
  filterHeroes: [],
  selectedHero: {}
};

function rootReducer(state = initialState, action) {
  switch (action.type) {
    case GET_HEROES_SUCCESS:
      if(JSON.stringify(state.heroes) === JSON.stringify(action.payload)){
        return state;
      }
      return { ...state, heroes: [...action.payload] };
    case FILTER_HEROES_SUCCESS:
      if(!action.payload){
        return { ...state, filterHeroes: [] };
      }
      var filterHeros = state.heroes.filter(x => {
        return (
          x.name
            .toLocaleLowerCase()
            .indexOf(action.payload.toLocaleLowerCase()) >= 0
        );
      });
      return { ...state, filterHeroes: filterHeros };
    case SELECT_HEROES_SUCCESS:
      return { ...state, selectedHero:action.payload };
    case ADD_HEROES_SUCCESS:
      return state;
    case DELETE_HEROES_SUCCESS:
      return { ...state};
    case UPDATE_HEROES_NAME_SUCCESS:
        return { ...state};
    default:
      return state;
  }
}
export default rootReducer;
