'use strict';

const dynamodb = require('./dynamodb');

module.exports.updatehero = (event, context, callback) => {
  const data = JSON.parse(event.body);

  // validation
//   if (typeof data.name !== 'string') {
//     console.error('Validation Failed');
//     callback(null, {
//       statusCode: 400,
//       headers: { 'Content-Type': 'text/plain' },
//       body: 'Couldn\'t update the todo item.',
//     });
//     return;
//   }

  const params = {
    TableName: "Heroes",
    Key: {
      id: +event.pathParameters.id,
    },
    ExpressionAttributeNames: {
      '#todo_name': 'name',
    },
    ExpressionAttributeValues: {
      ':name': data.name,
    },
    UpdateExpression: 'SET #todo_name = :name',
    ReturnValues: 'ALL_NEW',
  };

  // update the todo in the database
  dynamodb.update(params, (error, result) => {
    // handle potential errors
    if (error) {
      console.error(error);
      callback(null, {
        statusCode: error.statusCode || 501,
        headers: { 'Content-Type': 'text/plain' },
        body: 'Couldn\'t update the todo item.',
      });
      return;
    }

    // create a response
    const response = {
      statusCode: 200,
      body: JSON.stringify(result.Attributes),
    };
    callback(null, response);
  });
};
