import React from "react";
import { Link } from "react-router-dom";
import "./herodetail.css";
import { connect } from 'react-redux';
import { selectHeroes, updateNameHeroes } from "../redux/actions";


class HeroDetail extends React.Component {
  constructor(props) {
    super(props);
    this.changeHandler = this.changeHandler.bind(this);
  }

  changeHandler(e) {
   var hero = {
      id: this.props.selectedHero.id,
      name: e.target.value
    }
   this.props.dispatch(selectHeroes(hero));
  }

  updateHerohandler(){
    this.props.dispatch(updateNameHeroes(this.props.selectedHero));
  }

  componentDidMount() {}

  render() {
    return (
      <div className="herodetail">
        <h2>{this.props.selectedHero.name} Details</h2>
        <div>
          <span>id: </span>
          {this.props.selectedHero.id}
        </div>
        <div>
          <label>
            name:
            <input
              placeholder="name"
              value={this.props.selectedHero.name}
              onChange={this.changeHandler}
            />
          </label>
        </div>
        <Link to={this.props.screen}>
          <button>go back</button>
        </Link>
        <Link to="/dashboard">
          <button onClick={(event, item) => this.updateHerohandler(event)}>save</button>
        </Link>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
   return { selectedHero: state.selectedHero };
 };
 
 export default connect(mapStateToProps)(HeroDetail);
 