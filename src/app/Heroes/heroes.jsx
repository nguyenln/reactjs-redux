import React from "react";
import "./heroes.css";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { getHeroes, selectHeroes, addHeroes, deleteHeroes } from "../redux/actions";

class Heroes extends React.Component {
  // eslint-disable-next-line no-useless-constructor
  constructor(props) {
    super(props);
  }

  nameHero = "";

  componentDidMount() {
    this.props.dispatch(getHeroes());
  }

  componentDidUpdate(){
    this.props.dispatch(getHeroes());
  }

  deleteChange(event, idx) {
    this.props.dispatch(deleteHeroes(idx));
  }

  addHerohandler(event) {
    var id =
      this.props.heroes.length > 0
        ? Math.max(...this.props.heroes.map(hero => hero.id)) + 1
        : 11;
    this.props.dispatch(addHeroes({ id: id, name: this.nameHero }));
    var element = document.getElementsByClassName("inputName")[0];
    element.value = '';
  }

  changeNameHerohandler(event) {
    this.nameHero = event.target.value;
  }

  handlerChange(event, hero) {
    this.props.dispatch(selectHeroes(hero));
  }

  render() {
    var element = this.props.heroes.map(hero => {
      return (
        <li key={hero.id}>
          <Link
            to="/heroesdetail"
            onClick={(event, item) => this.handlerChange(event, hero)}
          >
            <span className="badge">{hero.id}</span> {hero.name}
          </Link>
          <button
            className="delete"
            title="delete hero"
            onClick={(event, item) => this.deleteChange(event, hero.id)}
          >
            x
          </button>
        </li>
      );
    });
    return (
      <div>
        <h2>My Heroes</h2>
        <label>
          Hero name:
          <input className='inputName' onChange={event => this.changeNameHerohandler(event)}></input>
          <button onClick={(event, item) => this.addHerohandler(event)}>
            add
          </button>
        </label>
        <ul className="heroes">{element}</ul>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return { heroes: state.heroes };
};

export default connect(mapStateToProps)(Heroes);
