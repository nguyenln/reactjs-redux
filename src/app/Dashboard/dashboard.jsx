import React from "react";
import "./dashboard.css";
import HeroSearch from "../HeroSearch/herosearch";
import {Link} from "react-router-dom";
import { connect } from 'react-redux';
import { getHeroes, selectHeroes } from '../redux/actions/index';

class Dashboard extends React.Component {
  // eslint-disable-next-line no-useless-constructor
  constructor(props) {
    super(props);
  }

  handlerChange(event,hero){
    this.props.dispatch(selectHeroes(hero));
  }

  componentDidMount() {
    this.props.dispatch(getHeroes());
  }
  render() {
    var element = this.props.heroes.slice(1, 5).map(hero => {
      return (
          <div key={hero.id}>
            <Link  to="/heroesdetail"
              onClick={(event,item) => this.handlerChange(event,hero) } className="col-1-4">
                <div className="module hero">
                    <h4>{hero.name}</h4>
                </div>
            </Link>
        </div>
      );
    });
    return (
      <div>
        <h3>Top Heroes</h3>
        <div className="grid grid-pad">{element}</div>
        <HeroSearch />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return { heroes: state.heroes };
};

export default connect(mapStateToProps)(Dashboard);

